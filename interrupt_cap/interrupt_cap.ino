/*
   interrupt_cap.ino

   Jacob Kunnappally
   October 2017

   What this program does:

   Setup
   -defines, globals, serial init. the usual
   -attach interrupt to an input pin

   Runtime
   -do nothing until status output LED is switched to
   another pin (done by interrupt routine)
   -shutdown and turn off LEDs after a few seconds
*/

#define STATUS_GOOD_PIN 9
#define ALARM_IN_PIN 0
#define ALARM_OUT_PIN 8

int out_led = STATUS_GOOD_PIN;
int counter = 0;

void setup()
{
  pinMode(STATUS_GOOD_PIN, OUTPUT);
  pinMode(ALARM_IN_PIN, INPUT);
  pinMode(ALARM_OUT_PIN, OUTPUT);
  attachInterrupt(ALARM_IN_PIN, switch_state, RISING);

  // initialize serial
  Serial.begin(115200);
  while (!Serial);
  Serial.flush();
  Serial.println("Starting...");
}

void loop()
{
  delay(1000);
  toggle_led(out_led);

  if (out_led == ALARM_OUT_PIN)
  {
    counter++;
    if (counter == 1) //print the first time
      Serial.println("!!!WARNING!!!HIGH RADIATION!!!");
    else if (counter > 10) //hypothetical time to put
    {                      //system in safe state
      Serial.println("Going into shutdown...");
      digitalWrite(out_led, false);
      detachInterrupt(ALARM_IN_PIN);
      while (1 < 2);
    }
  }
  else
  {
    //will never get here, but leaving things flexible
    counter = 0;
  }
}

void toggle_led(int pin)
{
  digitalWrite(pin, !digitalRead(pin));
}

void switch_state()
{
  digitalWrite(out_led, false);
  out_led = ALARM_OUT_PIN;
}

